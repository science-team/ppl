Source: ppl
Maintainer: Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Tobias Hansen <thansen@debian.org>,
           Michael Tautschnig <mt@debian.org>
Section: libs
Priority: optional
Build-Depends: debhelper (>= 11~),
               libgmp-dev,
               libncurses-dev
Build-Depends-Arch: chrpath,
                    swi-prolog [amd64 arm64 armel armhf hppa i386 ia64 mips mips64 mips64el mipsel powerpc powerpcspe ppc64 ppc64el] <!pkg.ppl.no-swi-prolog>
Build-Depends-Indep: doxygen-latex,
                     ghostscript,
                     graphviz,
                     poppler-utils,
                     swi-prolog,
                     texlive-science
Standards-Version: 4.5.0
Rules-Requires-Root: no
Vcs-Browser: https://salsa.debian.org/science-team/ppl
Vcs-Git: https://salsa.debian.org/science-team/ppl.git
Homepage: http://www.cs.unipr.it/ppl/

Package: libppl14
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends},
         ${shlibs:Depends}
Pre-Depends: ${misc:Pre-Depends}
Description: Parma Polyhedra Library (runtime library)
 The Parma Polyhedra Library (PPL) is a C++ library for the
 manipulation of (not necessarily closed) convex polyhedra and other
 numerical abstractions.  The applications of convex polyhedra include
 program analysis, optimized compilation, integer and combinatorial
 optimization and statistical data-editing.  The Parma Polyhedra
 Library is user friendly (you write `x + 2*y + 5*z <= 7' when you
 mean it), fully dynamic (available virtual memory is the only
 limitation to the dimension of anything), written in standard C++,
 exception-safe, rather efficient and thoroughly documented.

Package: libppl-dev
Architecture: any
Multi-Arch: same
Section: libdevel
Depends: libppl-c4 (= ${binary:Version}),
         libppl14 (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends}
Recommends: libgmp-dev
Suggests: libppl-doc
Pre-Depends: ${misc:Pre-Depends}
Breaks: libppl7 (<< 0.10~pre27-4),
        libppl9
Conflicts: libppl0.10-dev,
           libppl0.11-dev,
           libppl0.12-dev
Provides: libppl-c-dev,
          libppl0.12-dev
Replaces: libppl0.12-dev,
          libppl7 (<< 0.10~pre27-4),
          libppl9
Description: Parma Polyhedra Library (development)
 The Parma Polyhedra Library (PPL) is a C++ library for the
 manipulation of (not necessarily closed) convex polyhedra and other
 numerical abstractions.  The applications of convex polyhedra include
 program analysis, optimized compilation, integer and combinatorial
 optimization and statistical data-editing.  The Parma Polyhedra
 Library is user friendly (you write `x + 2*y + 5*z <= 7' when you
 mean it), fully dynamic (available virtual memory is the only
 limitation to the dimension of anything), written in standard C++,
 exception-safe, rather efficient and thoroughly documented.
 .
 This package provides the header files and static libraries for the
 C and C++ interfaces.

Package: ppl-dev
Architecture: any
Section: devel
Depends: libppl-dev (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends}
Pre-Depends: ${misc:Pre-Depends}
Replaces: libppl0.11-dev
Description: Parma Polyhedra Library (development binaries)
 The Parma Polyhedra Library (PPL) is a C++ library for the
 manipulation of (not necessarily closed) convex polyhedra and other
 numerical abstractions.  The applications of convex polyhedra include
 program analysis, optimized compilation, integer and combinatorial
 optimization and statistical data-editing.  The Parma Polyhedra
 Library is user friendly (you write `x + 2*y + 5*z <= 7' when you
 mean it), fully dynamic (available virtual memory is the only
 limitation to the dimension of anything), written in standard C++,
 exception-safe, rather efficient and thoroughly documented.
 .
 This package provides the ppl-config binary.

Package: libppl-c4
Architecture: any
Multi-Arch: same
Depends: libppl14 (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends}
Pre-Depends: ${misc:Pre-Depends}
Description: Parma Polyhedra Library (C interface)
 The Parma Polyhedra Library (PPL) is a C++ library for the
 manipulation of (not necessarily closed) convex polyhedra and other
 numerical abstractions.  The applications of convex polyhedra include
 program analysis, optimized compilation, integer and combinatorial
 optimization and statistical data-editing.  The Parma Polyhedra
 Library is user friendly (you write `x + 2*y + 5*z <= 7' when you
 mean it), fully dynamic (available virtual memory is the only
 limitation to the dimension of anything), written in standard C++,
 exception-safe, rather efficient and thoroughly documented.
 .
 This package provides the C interface.

Package: libppl-doc
Architecture: all
Section: doc
Depends: libjs-jquery,
         ${misc:Depends}
Description: Parma Polyhedra Library: Documentation
 The Parma Polyhedra Library (PPL) is a C++ library for the
 manipulation of (not necessarily closed) convex polyhedra and other
 numerical abstractions.  The applications of convex polyhedra include
 program analysis, optimized compilation, integer and combinatorial
 optimization and statistical data-editing.  The Parma Polyhedra
 Library is user friendly (you write `x + 2*y + 5*z <= 7' when you
 mean it), fully dynamic (available virtual memory is the only
 limitation to the dimension of anything), written in standard C++,
 exception-safe, rather efficient and thoroughly documented.
 .
 This package provides the documentation.

Package: libppl-swi
Architecture: amd64 arm64 armel armhf hppa i386 ia64 mips mips64 mips64el mipsel powerpc powerpcspe ppc64 ppc64el
Build-Profiles: <!pkg.ppl.no-swi-prolog>
Multi-Arch: same
Depends: libppl14 (= ${binary:Version}),
         swi-prolog,
         ${misc:Depends},
         ${shlibs:Depends}
Pre-Depends: ${misc:Pre-Depends}
Description: Parma Polyhedra Library (SWI Prolog interface)
 The Parma Polyhedra Library (PPL) is a C++ library for the
 manipulation of (not necessarily closed) convex polyhedra and other
 numerical abstractions.  The applications of convex polyhedra include
 program analysis, optimized compilation, integer and combinatorial
 optimization and statistical data-editing.  The Parma Polyhedra
 Library is user friendly (you write `x + 2*y + 5*z <= 7' when you
 mean it), fully dynamic (available virtual memory is the only
 limitation to the dimension of anything), written in standard C++,
 exception-safe, rather efficient and thoroughly documented.
 .
 This package provides the SWI Prolog interface.
